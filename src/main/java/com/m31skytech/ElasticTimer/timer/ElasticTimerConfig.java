package com.m31skytech.ElasticTimer.timer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ElasticTimerConfig implements Serializable {
    public ElasticTimerConfig() {
    }

    private Map<String,ElasticTimerSingleConfig> elasticTimerSingleConfigMap=new HashMap<>();

    public Map<String, ElasticTimerSingleConfig> getElasticTimerSingleConfigMap() {
        return elasticTimerSingleConfigMap;
    }

    public void setElasticTimerSingleConfigMap(Map<String, ElasticTimerSingleConfig> elasticTimerSingleConfigMap) {
        this.elasticTimerSingleConfigMap = elasticTimerSingleConfigMap;
    }

    public static class ElasticTimerSingleConfig implements Serializable{
        public static final String INVOKE_DEFINE_TYPE_BEAN_CLASS="BEAN_CLASS";
        public static final String INVOKE_DEFINE_TYPE_BEAN_ID="BEAN_ID";
        public static final String INVOKE_DEFINE_TYPE_STATIC_CLASS="STATIC_CLASS";

        private String jobName="-";
        private String jobGroup="-";
        private String triggerName="-";
        private String triggerGroup="-";
        private String triggerCornExpress="-";
        private String triggerType="-";
        private String jobNeedConcurrent="-";
        private String ipRegex=".*";
        private String isRunning="-";
        private String invokeDefineTypeArgs ="-";
        private String invokeDefineType =INVOKE_DEFINE_TYPE_BEAN_CLASS;
        private String invokeMethodName="-";

        public ElasticTimerSingleConfig() {
        }

        public ElasticTimerSingleConfig(String jobName, String jobGroup, String triggerName, String triggerGroup) {
            this.jobName = jobName;
            this.jobGroup = jobGroup;
            this.triggerName = triggerName;
            this.triggerGroup = triggerGroup;
        }

        public String getIsRunning() {
            return isRunning;
        }

        public void setIsRunning(String isRunning) {
            this.isRunning = isRunning;
        }

        public String getJobName() {
            return jobName;
        }


        public String getJobGroup() {
            return jobGroup;
        }


        public String getTriggerName() {
            return triggerName;
        }


        public String getTriggerGroup() {
            return triggerGroup;
        }


        public String getTriggerCornExpress() {
            return triggerCornExpress;
        }

        public void setTriggerCornExpress(String triggerCornExpress) {
            this.triggerCornExpress = triggerCornExpress;
        }

        public String getTriggerType() {
            return triggerType;
        }

        public void setTriggerType(String triggerType) {
            this.triggerType = triggerType;
        }

        public String getJobNeedConcurrent() {
            return jobNeedConcurrent;
        }

        public void setJobNeedConcurrent(String jobNeedConcurrent) {
            this.jobNeedConcurrent = jobNeedConcurrent;
        }

        public String getIpRegex() {
            return ipRegex;
        }

        public void setIpRegex(String ipRegex) {
            this.ipRegex = ipRegex;
        }

        public void setJobName(String jobName) {
            this.jobName = jobName;
        }

        public void setJobGroup(String jobGroup) {
            this.jobGroup = jobGroup;
        }

        public void setTriggerName(String triggerName) {
            this.triggerName = triggerName;
        }

        public void setTriggerGroup(String triggerGroup) {
            this.triggerGroup = triggerGroup;
        }

        public String getInvokeMethodName() {
            return invokeMethodName;
        }

        public void setInvokeMethodName(String invokeMethodName) {
            this.invokeMethodName = invokeMethodName;
        }

        @Override
        public String toString() {
            return "ElasticTimerSingleConfig{" +
                    "jobName='" + jobName + '\'' +
                    ", jobGroup='" + jobGroup + '\'' +
                    ", triggerName='" + triggerName + '\'' +
                    ", triggerGroup='" + triggerGroup + '\'' +
                    ", triggerCornExpress='" + triggerCornExpress + '\'' +
                    ", triggerType='" + triggerType + '\'' +
                    ", jobNeedConcurrent='" + jobNeedConcurrent + '\'' +
                    ", ipRegex='" + ipRegex + '\'' +
                    ", isRunning='" + isRunning + '\'' +
                    ", invokeDefineTypeArgs='" + invokeDefineTypeArgs + '\'' +
                    ", invokeDefineType='" + invokeDefineType + '\'' +
                    ", invokeMethodName='" + invokeMethodName + '\'' +
                    '}';
        }

        public String getInvokeDefineTypeArgs() {
            return invokeDefineTypeArgs;
        }

        public void setInvokeDefineTypeArgs(String invokeDefineTypeArgs) {
            this.invokeDefineTypeArgs = invokeDefineTypeArgs;
        }

        public String getInvokeDefineType() {
            return invokeDefineType;
        }

        public void setInvokeDefineType(String invokeDefineType) {
            this.invokeDefineType = invokeDefineType;
        }

        @Override
        public boolean equals(Object obj) {
            if(null==obj)return false;
            return this.toString().equals(obj.toString());
        }
    }

    public static String parseKey(String jobGroupName,String jobName,String triggerGroupName,String triggerName){
        return "jobgroup["+jobGroupName+"]_jobname["+jobName+"]_triggergroup["+triggerGroupName+"]_triggername["+triggerName+"]";
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ElasticTimerConfig))return false;
        ElasticTimerConfig elasticTimerConfigTarget= (ElasticTimerConfig) obj;
        if(elasticTimerConfigTarget.getElasticTimerSingleConfigMap().size()!=this.getElasticTimerSingleConfigMap().size())return false;
        for(Map.Entry<String,ElasticTimerSingleConfig> entry:this.getElasticTimerSingleConfigMap().entrySet()){
            if(!elasticTimerConfigTarget.getElasticTimerSingleConfigMap().get(entry.getKey())
                    .equals(entry.getValue()))return false;
        }
        return true;
    }
}
