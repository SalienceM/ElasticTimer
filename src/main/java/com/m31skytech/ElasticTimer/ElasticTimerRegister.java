package com.m31skytech.ElasticTimer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

@Component
public class ElasticTimerRegister implements ApplicationContextAware, InitializingBean, DisposableBean, SmartLifecycle {
    static Logger logger= LoggerFactory.getLogger(ElasticTimerRegister.class);
    @Autowired
    ElasticTimerCuratorHandleService elasticTimerCuratorHandleService;
    @Autowired
    ElasticTimerServiceProxy elasticTimerServiceProxy;

    private static ApplicationContext applicationContext;

    @Override
    public void destroy() throws Exception {
        logger.info(ElasticTimerRegister.class+" instance destroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info(String.valueOf(elasticTimerCuratorHandleService));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }

    @Override
    public boolean isAutoStartup() {
        return false;
    }

    @Override
    public void stop(Runnable runnable) {

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }

    @Override
    public int getPhase() {
        return 0;
    }


    public ElasticTimerCuratorHandleService getElasticTimerCuratorHandleService() {
        return elasticTimerCuratorHandleService;
    }

    public void setElasticTimerCuratorHandleService(ElasticTimerCuratorHandleService elasticTimerCuratorHandleService) {
        this.elasticTimerCuratorHandleService = elasticTimerCuratorHandleService;
    }

    public ElasticTimerServiceProxy getElasticTimerServiceProxy() {
        return elasticTimerServiceProxy;
    }

    public void setElasticTimerServiceProxy(ElasticTimerServiceProxy elasticTimerServiceProxy) {
        this.elasticTimerServiceProxy = elasticTimerServiceProxy;
    }

    public static ApplicationContext getApplicationContext(){
        return applicationContext;
    }

}
