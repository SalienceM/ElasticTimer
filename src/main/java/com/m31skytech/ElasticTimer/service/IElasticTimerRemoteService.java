package com.m31skytech.ElasticTimer.service;

public interface IElasticTimerRemoteService {
    ETRSUpdateRes update(ETRSUpdateReq req);
    ETRSUpdateRes pull(ETRSUpdateReq req);
    ETRSUpdateRes push(ETRSUpdateReq req);
    ETRSUpdateRes clearRemote(ETRSUpdateReq req);
    ETRSSearchRes search(ETRSSearchReq req);
    ETRSListNodeRes listNode(ETRSListNodeReq req);
    ETRSCheckRes checkStatus(ETRSCheckReq req);
}