package com.m31skytech.ElasticTimer.service;

import com.m31skytech.ElasticTimer.timer.ElasticTimerConfig;

import java.io.Serializable;

public class ETRSUpdateReq implements Serializable {
    public static final String SYNC="SYNC";
    public static final String ADD="ADD";
    public static final String DEL="DEL";
    public static final String STOPALL="STOPALL";
    public static final String STARTALL="STARTALL";

    private final String transactionId;
    private String optType=SYNC;
    private ElasticTimerConfig elasticTimerConfig;

    public ETRSUpdateReq(String transactionId) {
        this.transactionId = transactionId;
    }
    public ETRSUpdateReq() {
        this.transactionId=String.valueOf(System.currentTimeMillis());
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public ElasticTimerConfig getElasticTimerConfig() {
        return elasticTimerConfig;
    }

    public void setElasticTimerConfig(ElasticTimerConfig elasticTimerConfig) {
        this.elasticTimerConfig = elasticTimerConfig;
    }
}
