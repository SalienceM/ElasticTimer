package com.m31skytech.ElasticTimer.service;

import java.io.Serializable;

public class ETRSUpdateRes implements Serializable {
    public static final String SUCCESS="1001";
    public static final String FAIL="2001";

    private final String resultCode;
    private String resultDetail="-";
    private final String transactionId;

    public ETRSUpdateRes(String resultCode, String transactionId) {
        this.resultCode = resultCode;
        this.transactionId = transactionId;
    }

    public ETRSUpdateRes(String resultCode, String resultDetail, String transactionId) {
        this.resultCode = resultCode;
        this.resultDetail = resultDetail;
        this.transactionId = transactionId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getResultDetail() {
        return resultDetail;
    }

    public void setResultDetail(String resultDetail) {
        this.resultDetail = resultDetail;
    }
}
