package com.m31skytech.ElasticTimer.service;

import com.m31skytech.ElasticTimer.timer.ElasticTimerConfig;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ETRSCheckRes implements Serializable {
    public static String STATUS_ALLSYNC="STATUS_ALLSYNC";
    public static String STATUS_PARTSYNC="STATUS_PARTSYNC";
    public static String STATUS_NONESYNC="STATUS_NONESYNC";
    public static String STATUS_UNDEFINE="STATUS_UNDEFINE";

    private String transactionId=System.currentTimeMillis()+"";
    private String status=STATUS_UNDEFINE;
    private int totalNodeCount=0;
    private int syncedNodeCount=0;
    private int unSyncedNodeCount=0;
    private ElasticTimerConfig remoteConfig;
    private List<String> synced=new ArrayList<>();
    private Map<String,ElasticTimerConfig> unSync=new HashMap<>();
    private String resultMessage="-";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ElasticTimerConfig getRemoteConfig() {
        return remoteConfig;
    }

    public void setRemoteConfig(ElasticTimerConfig remoteConfig) {
        this.remoteConfig = remoteConfig;
    }

    public List<String> getSynced() {
        return synced;
    }

    public void setSynced(List<String> synced) {
        this.synced = synced;
    }

    public Map<String, ElasticTimerConfig> getUnSync() {
        return unSync;
    }

    public void setUnSync(Map<String, ElasticTimerConfig> unSync) {
        this.unSync = unSync;
    }

    public int getTotalNodeCount() {
        return totalNodeCount;
    }

    public void setTotalNodeCount(int totalNodeCount) {
        this.totalNodeCount = totalNodeCount;
    }

    public int getSyncedNodeCount() {
        return syncedNodeCount;
    }

    public void setSyncedNodeCount(int syncedNodeCount) {
        this.syncedNodeCount = syncedNodeCount;
    }

    public int getUnSyncedNodeCount() {
        return unSyncedNodeCount;
    }

    public void setUnSyncedNodeCount(int unSyncedNodeCount) {
        this.unSyncedNodeCount = unSyncedNodeCount;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
