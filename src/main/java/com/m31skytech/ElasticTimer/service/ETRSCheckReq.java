package com.m31skytech.ElasticTimer.service;


import java.io.Serializable;

public class ETRSCheckReq implements Serializable {
    private String transactionId=System.currentTimeMillis()+"";

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
