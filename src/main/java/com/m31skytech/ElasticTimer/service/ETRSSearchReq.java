package com.m31skytech.ElasticTimer.service;

import java.io.Serializable;

public class ETRSSearchReq implements Serializable {
    public static final String REMOTE="REMOTE";
    public static final String LOCAL="LOCAL";
    private String SearchType=LOCAL;
    private String transactionId=System.currentTimeMillis()+"";

    public String getSearchType() {
        return SearchType;
    }

    public void setSearchType(String searchType) {
        SearchType = searchType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
