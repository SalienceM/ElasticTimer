package com.m31skytech.ElasticTimer.service;

import com.m31skytech.ElasticTimer.timer.ElasticTimerConfig;

import java.io.Serializable;

public class ETRSSearchRes implements Serializable {
    public static final String SUCCESS="1001";
    public static final String ERROR="2001";
    private String transactionId=System.currentTimeMillis()+"";
    private String searchType="-";
    private String resultCode=ERROR;
    private String resultMessage="-";
    private ElasticTimerConfig elasticTimerConfig;

    public ETRSSearchRes() {
    }

    public ETRSSearchRes(String searchType, String resultCode, String resultMessage, ElasticTimerConfig elasticTimerConfig) {
        this.searchType = searchType;
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
        this.elasticTimerConfig = elasticTimerConfig;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public ElasticTimerConfig getElasticTimerConfig() {
        return elasticTimerConfig;
    }

    public void setElasticTimerConfig(ElasticTimerConfig elasticTimerConfig) {
        this.elasticTimerConfig = elasticTimerConfig;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

}
