package com.m31skytech.ElasticTimer.service;

import com.m31skytech.ElasticTimer.ElasticTimerCuratorHandleService;
import com.m31skytech.ElasticTimer.timer.ElasticTimerQuarztHandleProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ElasticTimerRemoteService implements IElasticTimerRemoteService {
    Logger logger= LoggerFactory.getLogger(ElasticTimerRemoteService.class);
    @Autowired
    ElasticTimerQuarztHandleProxy elasticTimerQuarztHandleProxy;
    @Autowired
    ElasticTimerCuratorHandleService elasticTimerCuratorHandleService;
    @Override
    public ETRSUpdateRes update(ETRSUpdateReq req) {
        try {
            if(elasticTimerCuratorHandleService.syncTimerConfig(req.getElasticTimerConfig())){
                String updateNodesDetail=
                        elasticTimerCuratorHandleService.notifyElasticTimerServiceNodeUpdate();
                return new ETRSUpdateRes(ETRSUpdateRes.SUCCESS,req.getTransactionId(),updateNodesDetail);
            }
            return new ETRSUpdateRes(ETRSUpdateRes.SUCCESS,req.getTransactionId(),"NO NEED UPDATE");
        } catch (Exception e) {
            logger.error("error when ElasticTimerRemoteService.update",e);
            return new ETRSUpdateRes(ETRSUpdateRes.FAIL,req.getTransactionId(),e.getLocalizedMessage());
        }
    }

    @Override
    public ETRSUpdateRes pull(ETRSUpdateReq req) {
        try {
            if(null==elasticTimerQuarztHandleProxy.getRemoteElasticTimerConfig()){
                return new ETRSUpdateRes(ETRSUpdateRes.FAIL,"REMOTE IS NULL",req.getTransactionId());
            }else{
                elasticTimerCuratorHandleService.syncTimerConfig(elasticTimerQuarztHandleProxy.getRemoteElasticTimerConfig());
                String updateNodesDetail=
                        elasticTimerCuratorHandleService.notifyElasticTimerServiceNodeUpdate();
                return new ETRSUpdateRes(ETRSUpdateRes.SUCCESS,req.getTransactionId(),updateNodesDetail);
            }
        } catch (Exception e) {
            logger.error("error when ElasticTimerRemoteService.pull",e);
            return new ETRSUpdateRes(ETRSUpdateRes.FAIL,req.getTransactionId(),e.getLocalizedMessage());
        }
    }

    @Override
    public ETRSUpdateRes push(ETRSUpdateReq req) {
        try {
                elasticTimerCuratorHandleService.syncTimerConfig(elasticTimerQuarztHandleProxy.getLocalRuntimeElasticTimerConfig());
                String updateNodesDetail=
                        elasticTimerCuratorHandleService.notifyElasticTimerServiceNodeUpdate();
                return new ETRSUpdateRes(ETRSUpdateRes.SUCCESS,updateNodesDetail,req.getTransactionId());
        } catch (Exception e) {
            logger.error("error when ElasticTimerRemoteService.push",e);
            return new ETRSUpdateRes(ETRSUpdateRes.FAIL,e.getLocalizedMessage(),req.getTransactionId());
        }
    }

    @Override
    public ETRSUpdateRes clearRemote(ETRSUpdateReq req) {
        try{
            elasticTimerCuratorHandleService.dropRemoteConfig();
            return new ETRSUpdateRes(ETRSUpdateRes.SUCCESS,"clear remote Config success.",req.getTransactionId());
        }catch (Exception e){
            logger.error("error when ElasticTimerRemoteService.clearRemote",e);
            return new ETRSUpdateRes(ETRSUpdateRes.FAIL,e.getLocalizedMessage(),req.getTransactionId());
        }
    }


    @Override
    public ETRSSearchRes search(ETRSSearchReq req) {
        try{
            if(ETRSSearchReq.LOCAL.equals(req.getSearchType())){
                return new ETRSSearchRes(req.getSearchType(),ETRSSearchRes.SUCCESS,"SUCCESS",
                        elasticTimerQuarztHandleProxy.getLocalRuntimeElasticTimerConfig());

            }else if (ETRSSearchReq.REMOTE.equals(req.getSearchType())){
                return new ETRSSearchRes(req.getSearchType(),ETRSSearchRes.SUCCESS,"SUCCESS",
                        elasticTimerQuarztHandleProxy.getRemoteElasticTimerConfig());
            }else{
                return new ETRSSearchRes(req.getSearchType(),ETRSSearchRes.ERROR,"target search type not found",null);
            }
        }catch (Exception e){
            logger.error("error when search req >>",e);
            return new ETRSSearchRes(req.getSearchType(),ETRSSearchRes.ERROR,e.getMessage(),null);
        }


    }

    @Override
    public ETRSListNodeRes listNode(ETRSListNodeReq req) {
        return new ETRSListNodeRes(elasticTimerCuratorHandleService
                .getAllDubboCurrentAliveProviderNodes(req.getServiceUrl(),req.getAppOwner(),req.getAppName()));
    }



    @Override
    public ETRSCheckRes checkStatus(ETRSCheckReq req) {
        try {
        List<ElasticTimerCuratorHandleService.Service> serviceList=
                elasticTimerCuratorHandleService
                        .getAllDubboCurrentAliveProviderNodes(IElasticTimerRemoteService.class.getName(),
                                elasticTimerCuratorHandleService.getCurrentOwner(),elasticTimerCuratorHandleService.getCurrentAppname());

            ETRSCheckRes res=elasticTimerQuarztHandleProxy.checkServiceSync(serviceList);
            res.setTransactionId(req.getTransactionId());
            return res;
        } catch (Exception e) {
            logger.error("error when check sync status. >> ",e);
            ETRSCheckRes res=new ETRSCheckRes();
            res.setTransactionId(req.getTransactionId());
            res.setResultMessage(e.getLocalizedMessage());
            return new ETRSCheckRes();
        }

    }


}
