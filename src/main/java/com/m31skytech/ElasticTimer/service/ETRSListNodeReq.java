package com.m31skytech.ElasticTimer.service;

import java.io.Serializable;

public class ETRSListNodeReq implements Serializable {
    private String appName=null;
    private String appOwner=null;
    private String serviceUrl=IElasticTimerRemoteService.class.getName();

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppOwner() {
        return appOwner;
    }

    public void setAppOwner(String appOwner) {
        this.appOwner = appOwner;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
}
