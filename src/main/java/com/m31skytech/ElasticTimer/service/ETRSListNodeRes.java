package com.m31skytech.ElasticTimer.service;

import com.m31skytech.ElasticTimer.ElasticTimerCuratorHandleService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ETRSListNodeRes implements Serializable {
    private List<ElasticTimerCuratorHandleService.Service> services=new ArrayList<>();

    public ETRSListNodeRes(List<ElasticTimerCuratorHandleService.Service> services) {
        this.services = services;
    }

    public ETRSListNodeRes() {
    }

    public List<ElasticTimerCuratorHandleService.Service> getServices() {
        return services;
    }

    public void setServices(List<ElasticTimerCuratorHandleService.Service> services) {
        this.services = services;
    }
}
