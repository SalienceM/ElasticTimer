package com.m31skytech.ElasticTimer;

import com.m31skytech.ElasticTimer.timer.ElasticTimerConfig;
import com.m31skytech.ElasticTimer.timer.ElasticTimerQuarztHandleProxy;
import com.alibaba.fastjson.JSON;
import com.netflix.curator.framework.CuratorFramework;
import com.netflix.curator.framework.recipes.cache.PathChildrenCacheEvent;
import com.netflix.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ElasticTimeTagListener implements PathChildrenCacheListener {
    Logger logger= LoggerFactory.getLogger(ElasticTimeTagListener.class);
    public ElasticTimeTagListener(){
        logger.info("ElasticTimeTagListener init.");
    }
    @Override
    public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
        //该节点仅处理更新事件
        if(null!=pathChildrenCacheEvent && null!=pathChildrenCacheEvent.getType()&& pathChildrenCacheEvent.getType().equals(PathChildrenCacheEvent.Type.CHILD_UPDATED)){
            logger.info("节点更新:"+pathChildrenCacheEvent.toString());
            ElasticTimerConfig configRemote=ElasticTimerRegister.getApplicationContext().getBean(ElasticTimerQuarztHandleProxy.class)
                    .getRemoteElasticTimerConfig();
            logger.info("R>>"+JSON.toJSONString(configRemote));
            ElasticTimerConfig configLocal=
            ElasticTimerRegister.getApplicationContext().getBean(ElasticTimerQuarztHandleProxy.class).getLocalRuntimeElasticTimerConfig();
            // test all
            logger.info("L>>"+JSON.toJSONString(configLocal));
            logger.info(configLocal
                    .equals(configRemote)+"");
            ElasticTimerRegister.getApplicationContext().getBean(ElasticTimerQuarztHandleProxy.class)
                    .syncConfigFromRemote(configLocal,configRemote);
        }
    }
}
