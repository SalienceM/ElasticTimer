package com.m31skytech.ElasticTimer;

import com.netflix.curator.framework.CuratorFramework;
import com.netflix.curator.framework.state.ConnectionState;
import com.netflix.curator.framework.state.ConnectionStateListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElasticTimerConnectionListener implements ConnectionStateListener {
    Logger logger= LoggerFactory.getLogger(ElasticTimerConnectionListener.class);
    @Override
    public void stateChanged(CuratorFramework curatorFramework, ConnectionState state) {

        if (state == ConnectionState.LOST) {
            //连接丢失
            logger.info("lost session with zookeeper");
        } else if (state == ConnectionState.CONNECTED) {
            //连接新建
            logger.info("connected with zookeeper");
        } else if (state == ConnectionState.RECONNECTED) {
            logger.info("reconnected with zookeeper");
            //连接重连
//                ElasticTimerRegister.getApplicationContext().getBean(ElasticTimerServiceProxy.class)
//                        .afterPropertiesSet();
        }else{
        }
    }
}
