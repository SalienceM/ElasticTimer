package com.m31skytech.ElasticTimer.utils;

import java.util.HashSet;
import java.util.Set;

public class CollectionsHelper {
    public static Merge merge(Set<String> originSet,Set<String> targetSet){
        Set<String> total=new HashSet<>();
        total.addAll(originSet);
        total.addAll(targetSet);
        Set<String> toAdd=new HashSet<>();
        Set<String> isSame=new HashSet<>();
        Set<String> toDel=new HashSet<>();
        for (String s : total) {
            if(originSet.contains(s) &&targetSet.contains(s)){
                isSame.add(s);continue;
            }
            if(originSet.contains(s) && !targetSet.contains(s)){
                toDel.add(s);continue;
            }
            if(!originSet.contains(s) && targetSet.contains(s)){
                toAdd.add(s);continue;
            }
        }
        return new Merge(toAdd,isSame,toDel);
    }

    public static class Merge{
        private Set<String> addSet=new HashSet<>();
        private Set<String> sameSet=new HashSet<>();
        private Set<String> delSet=new HashSet<>();

        public Merge(Set<String> addSet, Set<String> sameSet, Set<String> delSet) {
            this.addSet = addSet;
            this.sameSet = sameSet;
            this.delSet = delSet;
        }

        public Set<String> getAddSet() {
            return addSet;
        }

        public void setAddSet(Set<String> addSet) {
            this.addSet = addSet;
        }

        public Set<String> getSameSet() {
            return sameSet;
        }

        public void setSameSet(Set<String> sameSet) {
            this.sameSet = sameSet;
        }

        public Set<String> getDelSet() {
            return delSet;
        }

        public void setDelSet(Set<String> delSet) {
            this.delSet = delSet;
        }
    }
}
