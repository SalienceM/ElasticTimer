package com.m31skytech.ElasticTimer.utils;

import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.common.utils.StringUtils;

public class ZookeeperHelper {
    /**转换为标准注册中心路径
     *
     * @param urlStr
     * @return
     */
    public static String convertToZookeeperString(String urlStr) {
        URL url = URL.valueOf(urlStr);
        StringBuilder zkString = new StringBuilder();
        zkString.append(url.getHost() + ":" + url.getPort());
        if (StringUtils.isNotEmpty(url.getParameter("backup"))) {
            zkString.append(",").append(url.getParameter("backup"));
        }
        return zkString.toString();
    }

}
