package com.m31skytech;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DemoBean {
    //CONFIG FALSE,REMOTE TRUE CAN'T RUN



    Logger logger= LoggerFactory.getLogger(DemoBean.class);
    private int add=0;
    public void test() {
        String string=add+++"";
        try {
            logger.info("start "+string);
             Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end "+string);
    }


}

