package com.m31skytech;

import static org.junit.Assert.assertTrue;

import com.m31skytech.ElasticTimer.service.ETRSSearchReq;
import com.m31skytech.ElasticTimer.service.ETRSUpdateReq;
import com.m31skytech.ElasticTimer.timer.ElasticTimerConfig;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    public static void main(String[] args) throws IOException {
//        Main.main(new String[]{});
        ClassPathXmlApplicationContext context2 = new ClassPathXmlApplicationContext("classpath:config/spring/*.xml");
        context2.start();
        System.in.read();
    }

    @Test
    public void test(){
        ETRSUpdateReq req=new ETRSUpdateReq();
        ElasticTimerConfig elasticTimerConfig=new ElasticTimerConfig();
        ElasticTimerConfig.ElasticTimerSingleConfig singleConfig=new ElasticTimerConfig.ElasticTimerSingleConfig("DemoMethodInvoker", "DEFAULT", "DemoTrigger", "DEFAULT");
        singleConfig.setIpRegex(".*");
        singleConfig.setInvokeDefineTypeArgs("com.m31skytech.DemoBean");
        singleConfig.setInvokeMethodName("test");
        singleConfig.setIsRunning("STATUS_RUNNING");
        singleConfig.setJobNeedConcurrent("true");
        singleConfig.setTriggerCornExpress("0/5 * * * * ?");
        singleConfig.setTriggerType("org.springframework.scheduling.quartz.CronTriggerBean");
        elasticTimerConfig.getElasticTimerSingleConfigMap().put(ElasticTimerConfig.parseKey("DEFAULT","DemoMethodInvoker","DEFAULT","DemoTrigger")
        ,singleConfig);
        req.setElasticTimerConfig(elasticTimerConfig);
        System.out.println(JSON.toJSONString(req));
    }


    @Test
    public void testRetain(){
        Set<String> stringSetA=new HashSet<>();
        Set<String> stringSetACopy=new HashSet<>();
        Set<String> stringSetACopy2=new HashSet<>();

        Set<String> stringSetB=new HashSet<>();
        Set<String> stringSetBCopy=new HashSet<>();
        Set<String> stringSetBCopy2=new HashSet<>();
//        stringSetA.add("1");
//        stringSetA.add("2");
//        stringSetB.add("2");
//        stringSetB.add("3");
        stringSetA.add("1");
        stringSetA.add("2");
        stringSetB.add("1");
        stringSetB.add("4");

        stringSetACopy2.addAll(stringSetA);
        stringSetACopy.addAll(stringSetA);
        stringSetBCopy.addAll(stringSetB);
        stringSetBCopy2.addAll(stringSetB);

        stringSetA.retainAll(stringSetB);
        stringSetACopy.removeAll(stringSetA);
        stringSetBCopy.retainAll(stringSetACopy2);
        stringSetBCopy2.removeAll(stringSetBCopy);

        //A PLUS
        System.out.println(stringSetACopy);
        //B OCCUR
        System.out.println(stringSetA);
        //C DEL
        System.out.println(stringSetBCopy2);
    }

    @Test
    public void testETRSSearchRQ(){
        ETRSSearchReq req=new ETRSSearchReq();
        req.setSearchType("LOCAL");
        System.out.println(JSON.toJSONString(req));
    }



}
