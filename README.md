# ElasticTimer

#### 介绍
使用zookeeper协同的分布式动态定时器，可随时加载卸载任务，暴露dubbo服务进行管理行为，无需额外存储


#### 软件架构
spring:生命周期管理与Quarzt整合
quarzt:定时任务实现
zookeeper:任务协调
dubbo:定时器相关服务管理
curator-framework: zookeeper 客户端，封装监听，注册加载节点

#### 安装教程
##### 版本说明
你需要按照当前的项目进行版本选择，以下表格会进行实时更新

JDK版本|ElasticTimer版本   |Spring版本   |zookeeper版本   |curator版本   |dubbo版本|quarzt版本|功能|更新时间|
| ------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ------------ | ------------ |
|1.7|1.0.0   |3.1.0.RELEASE   |3.3.3   |1.0.6   |2.5.3|1.6.3|动态加载、卸载任务、dubbo服务(更新、添加、删除、同步)、配置持久化、无外部存储|2020-04-29 13:06:26 星期三|





##### 基于Spring 安装
1. 下载最新或指定分支版本源码
2. 进行maven编译(国内可配置 ali 源,资源会比较全) `mvn clean install`
3. 代码中引入依赖

```
    <dependency>
      <groupId>com.m31skytech</groupId>
      <artifactId>ElasticTimer</artifactId>
      <version>${targetVersion}</version>
    </dependency>  
```
    
暂时不提供非依赖Spring版本

#### 使用说明
1. 在任一Spring配置文件中添加包扫描声明
`    <context:component-scan base-package="com.m31skytech.ElasticTimer" />`

2. 编写基于Spring Quarzt 的配置文件，该部分可参考Quartz 官方配置指南 
[http://www.quartz-scheduler.org/](http://www.quartz-scheduler.org/)

3. 将所使用的 `org.springframework.scheduling.quartz.SchedulerFactoryBean` 进行如下配置引入
```java
    <bean id="quarztHandleProxy" class="com.m31skytech.ElasticTimer.timer.ElasticTimerQuarztHandleProxy">
        <property name="innerSchedule" ref="scheduleFactory"/>
    </bean>
```

4. 在资源目录下新建 `elastictimer.properties` 文件，按照以下参数列表进行填写

|   属性名|可空   |描述   |
| ------------ | ------------ | ------------ |
|elastictimer.config.type  | N  | 配置方式,目前支持2种,代理或直连 proxy/direct  |
|elastictimer.config.proxy.filename   | Y  | **使用 proxy 模式时非空**,表示所代理的属性值文件全名该文件应该放在 resources 目录下，例:dubbo.properties  |
|elastictimer.config.proxy.zookeeper.path   |Y   |**使用 proxy 模式时非空**,表示代理配置文件中的代表zookeeper 注册中心地址的配置属性名,例:在dubbo.properties 中存在以下配置   dubbo.registry.address=zookeeper://127.0.0.1:2181 则应该填写 `elastictimer.config.proxy.zookeeper.path=dubbo.registry.address`|
|elastictimer.config.proxy.application.code   |Y   |**使用 proxy 模式时非空**,表示代理配置文件中的代表dubbo application code的配置属性名   |
|elastictimer.config.proxy.owner.code   | Y  |**使用 proxy 模式时非空**,表示代理配置文件中的代表dubbo owner code的配置属性名   |
|elastictimer.config.proxy.dubbo.port   |Y   |**使用 proxy 模式时非空**,表示代理配置文件中的代表dubbo port的配置属性名   |
|elastictimer.zookeeper.path   |Y   |**使用 direct 模式时应填写**，表示所链接的注册中心值，默认值为： `zookeeper://127.0.0.1:2181`  |
|elastictimer.application.code   |Y   |**使用 direct 模式时应填写**，表示dubbo服务所使用的appname，默认值为 `ElasticTimerApp`   |
|elastictimer.owner.code   |Y   |**使用 direct 模式时应填写**，表示dubbo服务所使用的ownercode，默认值为 `M31`   |
|elastictimer.dubbo.port   |Y   |**使用 direct 模式时应填写**，表示dubbo服务所使用的端口，默认值为 `20880`   ||


#### 参与贡献

1.  SlienceM



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
